/*
 * Copyright © 2018-2019 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import Ergo 0.0
import QtQuick 2.7
import QtQuick.Layouts 1.1
import OAth 1.0
import QtMultimedia 5.8
import QtQuick.Controls 2.2
import QtQuick.Window 2.0
import "."

Window {
    id: root

    title: i18n.tr("Authenticator")
    visible: true

    width: units.dp(400)
    height: units.dp(640)

    // Use these colors for the UI.
    readonly property color bgColor: "#232323"
    readonly property color fgColor: "#efefef"

    Gettext {
        domain: "authenticator-ng"
    }

    Clipboard {
        id: clipboard
    }

    StackView {
        id: pageStack
        anchors.fill: parent

        Component.onCompleted: pageStack.push(mainPage)
    }

    Page {
        id: mainPage
        visible: false

        header: Rectangle {
            id: mainToolbar
            width: mainPage.width
            height: units.dp(56)
            color: root.bgColor

            AdaptiveToolbar {
                anchors.fill: parent
                height: units.dp(56)

                trailingActions: [
                    Action {
                        color: root.fgColor
                        iconName: "camera-symbolic"
                        text: i18n.tr("Scan QR code")
                        onTriggered: {
                            pageStack.push(grabCodeComponent);
                        }
                    },
                    Action {
                        color: root.fgColor
                        iconName: "add"
                        text: i18n.tr("Add Account")
                        onTriggered: {
                            pageStack.push(editSheetComponent);
                        }
                    }
                ]
            }
        }

        Rectangle {
            anchors.fill: parent
            color: root.bgColor
        }

        Text {
            anchors.centerIn: parent
            width: parent.width - units.dp(40)
            text: i18n.tr("No account set up. Use the buttons in the toolbar to add accounts.")
            wrapMode: Text.WordWrap
            font.pixelSize: units.dp(24)
            horizontalAlignment: Text.AlignHCenter
            visible: accountsListView.count == 0 && pageStack.depth == 1
            color: root.fgColor
        }

        Popup {
            id: popover
            padding: units.dp(12)

            x: parent.width / 2 - width / 2
            y: parent.height - height - units.dp(12)

            background: Rectangle {
                color: "#111111"
                opacity: 0.93
                radius: units.dp(10)
            }

            Text {
                id: copiedLabel
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                color: "#ffffff"
                text: i18n.tr("Code copied to clipboard")
                font.pixelSize: units.dp(16)
            }

            Timer {
                id: popupTimer
                interval: 3000
                running: true
                onTriggered: {
                    popover.close();
                }
            }

            function show() {
                open();
                popupTimer.start();
            }
        }

        ListView {
            id: accountsListView
            anchors.fill: parent
            spacing: units.dp(8)
            model: accounts
            interactive: contentHeight > height

            delegate: AdaptiveListItem {
                id: accountDelegate
                width: parent.width
                height: units.dp(56)

                property bool activated: false

                actions: [
                    Action {
                        iconName: "edit-copy"
                        text: i18n.tr("Copy")
                        enabled: accountDelegate.activated || type === Account.TypeTOTP
                        onTriggered: {
                            accountDelegate.copyToClipBoard()
                        }
                    },
                    Action {
                        iconName: "edit"
                        text: i18n.tr("Edit")
                        onTriggered: {
                            pageStack.push(editSheetComponent, {account: accounts.get(index)})
                        }
                    },
                    Action {
                        iconName: "delete"
                        text: i18n.tr("Remove")
                        onTriggered: {
                            var popup = removeQuestionComponent.createObject(
                                root,
                                {account: accounts.get(index)});
                            popup.accepted.connect(function() {
                                accounts.deleteAccount(index);
                            });
                            popup.rejected.connect(function() {
                                accounts.refresh();
                            });
                            popup.open();
                        }
                    }
                ]

                function copyToClipBoard() {
                    clipboard.pushData(otpLabel.text);
                    popover.show();
                }

                GridLayout {
                    id: delegateColumn
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                        leftMargin: units.dp(16)
                        topMargin: units.dp(8)
                        rightMargin: refreshButton.width + units.dp(24)
                    }
                    rowSpacing: units.dp(4)
                    columnSpacing: units.dp(8)
                    height: parent.height - (anchors.topMargin * 2)
                    columns: 1

                    Text {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        text: name
                        font.pixelSize: units.dp(14)
                        elide: Text.ElideRight
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        color: root.fgColor
                    }

                    Text {
                        id: otpLabel
                        Layout.fillHeight: true
                        Layout.preferredWidth: delegateColumn.width
                        color: root.fgColor
                        font.family: "mono"
                        font.pixelSize: units.dp(22)
                        text: accountDelegate.activated || type === Account.TypeTOTP ? otp : "------"
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        anchors.left: parent.left

                        MouseArea {
                            id: copy
                            anchors {
                                left: parent.left
                                bottom: parent.bottom
                            }
                            width: parent.contentWidth
                            height: parent.contentHeight
                            onClicked: {
                                accountDelegate.copyToClipBoard()
                            }
                        }
                    }
                }

                Item {
                    id: refreshButton
                    anchors {
                        right: parent.right
                        rightMargin: units.dp(8)
                        verticalCenter: parent.verticalCenter
                    }
                    height: parent.height
                    width: height

                    Icon {
                        anchors.centerIn: parent
                        name: "reload"
                        visible: type === Account.TypeHOTP
                        height: parent.height - units.dp(16)
                        width: height
                        color: root.fgColor
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                accounts.generateNext(index);
                                accountDelegate.activated = true;
                            }
                        }
                    }

                    Item {
                        id: progressCircle
                        anchors.centerIn: parent
                        height: parent.height - units.dp(16)
                        width: height
                        visible: type === Account.TypeTOTP
                        property real progress: 0

                        Timer {
                            interval: 100
                            running: type === Account.TypeTOTP
                            repeat: true
                            onTriggered: {
                                var duration = accounts.get(index).msecsToNext();
                                progressCircle.progress = ((timeStep * 1000) - duration) / (timeStep * 1000)
                            }
                        }

                        Canvas {
                            id: canvas
                            anchors.fill: parent
                            rotation: -90

                            property real scale: Screen.devicePixelRatio >= 1.0 ? 1.0 : Screen.devicePixelRatio

                            property real progress: progressCircle.progress
                            onProgressChanged: {
                                canvas.requestPaint();
                            }

                            onPaint: {
                                var ctx = canvas.getContext("2d");
                                ctx.save();
                                ctx.reset();
                                var data = [1 - progress, progress];
                                var myTotal = 0;

                                for (var e = 0; e < data.length; e++) {
                                    myTotal += data[e];
                                }

                                ctx.fillStyle = root.fgColor;

                                ctx.beginPath();
                                ctx.moveTo(canvas.width / 2 * scale, canvas.height / 2 * scale);
                                ctx.arc(canvas.width / 2 * scale, canvas.height / 2 * scale,
                                        canvas.height / 2 * scale, 0,
                                        (Math.PI * 2 * ((1 - progress) / myTotal)),
                                        false);
                                ctx.lineTo(canvas.width / 2 * scale, canvas.height / 2 * scale);
                                ctx.closePath();
                                ctx.fill();

                                ctx.restore();
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            anchors.top: parent.top
            color: "#343434"
            width: parent.width
            height: units.dp(2)
        }
    }

    Component {
        id: editSheetComponent
        Page {
            id: editPage
            property QtObject account: null
            readonly property bool validData: {
                return (nameField.displayText.length > 0 &&
                        secretField.displayText.length >= 16);
            }

            header: Rectangle {
                id: editToolbar
                width: editPage.width
                height: units.dp(56)
                color: root.bgColor

                AdaptiveToolbar {
                    anchors.fill: parent
                    height: units.dp(56)
                    leadingActions: [
                        Action {
                            color: root.fgColor
                            iconName: "back"
                            onTriggered: {
                                pageStack.pop();
                            }
                        }
                    ]
                    trailingActions: [
                        Action {
                            color: editPage.validData ? root.fgColor : "#717171"
                            iconName: "tick"
                            text: i18n.tr("Save account")
                            onTriggered: {
                                var newAccount = account;
                                if (newAccount == null) {
                                    newAccount = accounts.createAccount();
                                }

                                newAccount.name = nameField.text;
                                newAccount.type = typeSelector.currentIndex == 1 ? Account.TypeTOTP : Account.TypeHOTP;
                                newAccount.secret = secretField.text;
                                newAccount.counter = parseInt(counterField.text);
                                newAccount.timeStep = parseInt(timeStepField.text);
                                newAccount.pinLength = parseInt(pinLengthField.text);
                                pageStack.pop();
                            }
                        }
                    ]
                    Text {
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        text: account == null ? i18n.tr("Add account") : i18n.tr("Edit account")
                        color: root.fgColor
                        font.pixelSize: units.dp(24)
                    }
                }
            }

            Rectangle {
                anchors.fill: parent
                color: root.bgColor
            }

            Rectangle {
                anchors.top: parent.top
                color: "#343434"
                width: parent.width
                height: units.dp(2)
            }

            Flickable {
                id: settingsFlickable
                anchors.fill: parent
                contentHeight: settingsColumn.height + (settingsColumn.anchors.margins * 2)

                Column {
                    id: settingsColumn
                    anchors {
                        left: parent.left
                        right: parent.right
                        top: parent.top
                        margins: units.dp(16)
                    }
                    spacing: units.dp(16)

                    Text {
                        text: i18n.tr("Name")
                        color: root.fgColor
                        font.pixelSize: units.dp(18)
                    }
                    TextField {
                        id: nameField
                        width: parent.width
                        height: units.dp(36)
                        font.pixelSize: units.dp(14)
                        text: account ? account.name : ""
                        color: root.fgColor
                        background: Rectangle {
                            anchors.fill: parent
                            color: "transparent"
                            border.color: "#5d5d5d"
                            radius: units.dp(10)
                        }
                        selectByMouse: true
                        placeholderText: i18n.tr("Enter the account name")
                        inputMethodHints: Qt.ImhNoPredictiveText
                    }

                    Text {
                        text: i18n.tr("Type")
                        color: root.fgColor
                        font.pixelSize: units.dp(18)
                    }

                    ComboBox {
                        id: typeSelector
                        width: parent.width
                        height: units.dp(36)
                        editable: false
                        model: [i18n.tr("Counter based"), i18n.tr("Time based")]
                        currentIndex: account && account.type === Account.TypeHOTP ? 0 : 1

                        delegate: ItemDelegate {
                            height: typeSelector.height - units.dp(8)
                            width: typeSelector.width - units.dp(12)
                            x: units.dp(4)
                            contentItem: Rectangle {
                                color: "transparent"
                                anchors.fill: parent
                                Text {
                                    anchors.fill: parent
                                    leftPadding: units.dp(4)
                                    color: root.fgColor
                                    text: modelData
                                    font.bold: typeSelector.highlightedIndex === index
                                    verticalAlignment: Text.AlignVCenter
                                    font.pixelSize: units.dp(14)
                                }
                            }
                            highlighted: false
                        }

                        indicator: Icon {
                            id: downIndicator
                            x: typeSelector.width - width - typeSelector.rightPadding
                            y: typeSelector.topPadding + (typeSelector.availableHeight - height) / 2
                            height: typeSelector.height / 2
                            width: height
                            name: "go-down"
                            color: root.fgColor
                        }

                        background: Rectangle {
                            anchors.fill: parent
                            color: "#232323"
                            border.color: "#5d5d5d"
                            border.width: units.dp(2)
                            radius: units.dp(10)
                        }

                        contentItem: Text {
                            anchors.fill: parent
                            leftPadding: units.dp(16)
                            padding: units.dp(4)
                            color: root.fgColor
                            text: typeSelector.displayText
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: units.dp(14)
                        }

                        popup: Popup {
                            y: typeSelector.height - 1
                            width: typeSelector.width
                            implicitHeight: contentItem.implicitHeight
                            padding: units.dp(2)

                            contentItem: ListView {
                                clip: true
                                implicitHeight: contentHeight
                                model: typeSelector.delegateModel
                                currentIndex: typeSelector.highlightedIndex
                                delegate: typeSelector.delegate
                            }

                            background: Rectangle {
                                width: typeSelector.width
                                implicitHeight: contentItem.implicitHeight
                                color: "#343434"
                                border.color: root.bgColor
                                border.width: units.dp(2)
                                radius: units.dp(10)
                            }
                        }
                    }

                    Text {
                        text: i18n.tr("Key")
                        color: root.fgColor
                        font.pixelSize: units.dp(18)
                    }
                    TextField {
                        id: secretField
                        width: parent.width
                        height: units.dp(36)
                        font.pixelSize: units.dp(14)
                        text: account ? account.secret : ""
                        color: root.fgColor
                        background: Rectangle {
                            anchors.fill: parent
                            color: "transparent"
                            border.color: "#5d5d5d"
                            radius: units.dp(10)
                        }
                        selectByMouse: true
                        wrapMode: Text.WrapAnywhere
                        // TRANSLATORS: placeholder text in key textfield
                        placeholderText: i18n.tr("Enter the 16 or 32 digit key")
                        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
                    }
                    Row {
                        width: parent.width
                        spacing: units.dp(4)
                        visible: typeSelector.currentIndex == 0

                        Text {
                            text: i18n.tr("Counter")
                            anchors.verticalCenter: parent.verticalCenter
                            color: root.fgColor
                            font.pixelSize: units.dp(18)
                        }
                        TextField {
                            id: counterField
                            text: account ? account.counter : 0
                            width: parent.width - x
                            height: units.dp(36)
                            font.pixelSize: units.dp(14)
                            inputMask: "0009"
                            inputMethodHints: Qt.ImhDigitsOnly
                            color: root.fgColor
                            background: Rectangle {
                                anchors.fill: parent
                                color: "transparent"
                                border.color: "#5d5d5d"
                                radius: units.dp(10)
                            }
                            selectByMouse: true
                        }
                    }
                    Row {
                        width: parent.width
                        spacing: units.dp(4)
                        visible: typeSelector.currentIndex == 1

                        Text {
                            text: i18n.tr("Time step")
                            anchors.verticalCenter: parent.verticalCenter
                            color: root.fgColor
                            font.pixelSize: units.dp(18)
                        }
                        TextField {
                            id: timeStepField
                            text: account ? account.timeStep : 30
                            width: parent.width - x
                            height: units.dp(36)
                            font.pixelSize: units.dp(14)
                            inputMask: "0009"
                            inputMethodHints: Qt.ImhDigitsOnly
                            color: root.fgColor
                            background: Rectangle {
                                anchors.fill: parent
                                color: "transparent"
                                border.color: "#5d5d5d"
                                radius: units.dp(10)
                            }
                            selectByMouse: true
                        }
                    }
                    Row {
                        width: parent.width
                        spacing: units.dp(4)

                        Text {
                            text: i18n.tr("PIN length")
                            anchors.verticalCenter: parent.verticalCenter
                            color: root.fgColor
                            font.pixelSize: units.dp(18)
                        }
                        TextField {
                            id: pinLengthField
                            text: account ? account.pinLength : 6
                            width: parent.width - x
                            height: units.dp(36)
                            font.pixelSize: units.dp(14)
                            inputMask: "0D"
                            inputMethodHints: Qt.ImhDigitsOnly
                            color: root.fgColor
                            background: Rectangle {
                                anchors.fill: parent
                                color: "transparent"
                                border.color: "#5d5d5d"
                                radius: units.dp(10)
                            }
                            selectByMouse: true
                        }
                    }
                    Item {
                        width: parent.width
                        height: Qt.inputMethod.keyboardRectangle.height
                    }
                }
            }

        }
    }

    AccountModel {
        id: accounts
    }

    Component {
        id: grabCodeComponent
        Page {
            id: grabCodePage

            header: Rectangle {
                width: grabCodePage.width
                height: units.dp(56)
                color: root.bgColor

                AdaptiveToolbar {
                    anchors.fill: parent
                    height: units.dp(56)
                    leadingActions: [
                        Action {
                            color: root.fgColor
                            iconName: "back"
                            onTriggered: {
                                pageStack.pop();
                            }
                        }
                    ]
                    Text {
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        text: i18n.tr("Scan QR code")
                        font.pixelSize: units.dp(24)
                        color: root.fgColor
                    }
                }
            }

            Rectangle {
                anchors.fill: parent
                color: root.bgColor
            }

            Rectangle {
                anchors.top: parent.top
                color: "#343434"
                width: parent.width
                height: units.dp(2)
            }

            QRCodeReader {
                id: qrCodeReader

                onValidChanged: {
                    if (valid) {
                        var account = accounts.createAccount();
                        account.name = qrCodeReader.accountName;
                        account.type = qrCodeReader.type;
                        account.secret = qrCodeReader.secret;
                        account.counter = qrCodeReader.counter;
                        account.timeStep = qrCodeReader.timeStep;
                        account.pinLength = qrCodeReader.pinLength;
                        pageStack.pop();
                    }
                }
            }

            Camera {
                id: camera

                captureMode: Camera.CaptureViewfinder

                focus.focusMode: Camera.FocusMacro + Camera.FocusContinuous
                focus.focusPointMode: Camera.FocusPointCenter

                exposure.exposureMode: Camera.ExposureBarcode
                exposure.meteringMode: Camera.MeteringSpot

                imageProcessing.sharpeningLevel: 0.5
                imageProcessing.denoisingLevel: 0.25

                viewfinder.minimumFrameRate: 30.0
                viewfinder.maximumFrameRate: 30.0

                function startAndConfigure() {
                    start();
                }


                Component.onCompleted: {
                    captureTimer.start()
                }
            }
            Connections {
                target: Qt.application
                onActiveChanged: if (Qt.application.active) camera.startAndConfigure()
            }

            Timer {
                id: captureTimer
                interval: 3000
                repeat: true
                onTriggered: {
                    print("capturing");
                    qrCodeReader.grab();
                }
                onRunningChanged: {
                    if (running) {
                        camera.startAndConfigure();
                    } else {
                        camera.stop();
                    }
                }
            }

            VideoOutput {
                id: viewfinder

                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
                source: camera
                focus: visible
                orientation: Screen.primaryOrientation == Qt.PortraitOrientation ? -90 : 0
            }

            Rectangle {
                anchors {
                    left: parent.left
                    top: parent.top
                    right: parent.right
                    margins: units.dp(4)
                }
                color: "#111111"
                opacity: 0.93
                radius: units.dp(12)
                height: scanLabel.height + units.dp(8)

                Text {
                    id: scanLabel
                    width: parent.width
                    padding: units.dp(4)
                    y: units.dp(4)
                    text: i18n.tr("Scan a QR Code containing account information")
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: units.dp(16)
                    color: root.fgColor
                }
            }
        }
    }

    Component {
        id: removeQuestionComponent

        Rectangle {
            id: removeQuestionRect

            property alias account: removeQuestionDialog.account

            signal accepted()
            signal rejected()

            function open() {
                removeQuestionDialog.open();
            }

            function close() {
                removeQuestionDialog.close();
            }

            height: parent.height
            width: parent.width

            color: "#000000"
            opacity: 0.88
            visible: removeQuestionDialog && removeQuestionDialog.opened

            Popup {
                id: removeQuestionDialog

                property QtObject account

                x: parent.width / 2 - width / 2
                y: parent.height / 2 - height / 2 - units.dp(35)

                width: parent.width - units.dp(24) * 2
                padding: units.dp(12)

                modal: true
                closePolicy: Popup.CloseOnEscape

                onClosed: {
                    destroy();
                }

                background: Rectangle {
                    color: "#111111"
                    opacity: 0.99
                    radius: units.dp(12)
                }

                ColumnLayout {
                    width: parent.width
                    spacing: units.dp(12)

                    Text {
                        Layout.fillWidth: true
                        text: i18n.tr("Remove account?")
                        color: "#efefef"
                        font.pixelSize: units.dp(30)
                        wrapMode: Text.WordWrap
                    }

                    Text {
                        Layout.fillWidth: true
                        text: i18n.tr("Are you sure you want to remove the '%1' account?").arg(account ? account.name : "")
                        color: "#efefef"
                        font.pixelSize: units.dp(16)
                        wrapMode: Text.WordWrap
                    }

                    Item {
                        Layout.fillWidth: true
                        height: units.dp(16)
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignRight
                        spacing: units.dp(24)

                        Button {
                            Layout.fillWidth: true
                            font.pixelSize: units.dp(16)
                            text: i18n.tr("Cancel")
                            onClicked: {
                                removeQuestionRect.rejected();
                                removeQuestionDialog.close();
                            }
                        }
                        Button {
                            Layout.fillWidth: true
                            font.pixelSize: units.dp(16)
                            text: i18n.tr("Remove")
                            onClicked: {
                                removeQuestionRect.accepted();
                                removeQuestionDialog.close();
                            }
                            background: Rectangle {
                                color: "#ed3434"
                                radius: units.dp(10)
                            }
                        }
                    }
                }
            }
        }
    }
}
